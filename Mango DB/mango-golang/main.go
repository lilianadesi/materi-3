package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/liliana/mango-golang/controllers"
	"gopkg.in/mgo.v2"
)

func main (){

	r := httprouter.New()
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:Sid", uc.DeleteUser)
	http.ListenAndServe("localhost:9000", r)
}

func getSession() *mgo.Session{

	s, err := mgo.Dial("mangodb://localhost:27107")
	if err != nil{
		panic(err)
	}
	return s
}