package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// book struct (model)
type Book  struct{
	ID 			string 		`json:"id"`
	Isbn 		string 		`json:"isbn"`
	Title 		string 		`json:"title"`
	Author 		*Author 	`json:"id"`
}

//init books var as a slice book struct
var books []Book


// Author strcut 
type Author struct { 
	Firstname	string		`json:"firsname"`
	Lastname	string		`json:"lastsname"`
}
// get all books
func getBooks(w http.ResponseWriter, r *http.Request){
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(books)

}
// get single book
func getBook(w http.ResponseWriter, r *http.Request){
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r) // get params
	// look throught books and find with id
	for _, item := range books {
		if item.ID == params["id"]{
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Book{})
}
// creat new book
func createBook(w http.ResponseWriter, r *http.Request){
	w.Header().Set("content-type", "application/json")
	var book Book
	_ = json.NewDecoder(r.Body).Decode(&book)
	book.ID = strconv.Itoa(rand.Intn(10000000)) // mpck id -not safe
	books = append(books, book)
	json.NewEncoder(w).Encode(book)
}
func UpdateBooks(w http.ResponseWriter, r *http.Request){
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	for index, item := range books {
		if item.ID == params ["id"] {
		books = append(books[:index], books[index+1:]...)
		var book Book
		_ = json.NewDecoder(r.Body).Decode(&book)
		book.ID = strconv.Itoa(rand.Intn(10000000)) // mpck id -not safe
		books = append(books, book)
		json.NewEncoder(w).Encode(book)
		return
		}
		}
		json.NewEncoder(w).Encode(books)
}
func DeleteBooks(w http.ResponseWriter, r *http.Request){
	w.Header().Set("content-type", "application/json")
	params  := mux.Vars(r)
	for index, item := range books {
		if item.ID == params["id"] {
		books = append(books[:index], books[index+1:]...)
		break
	}
	}
	json.NewEncoder(w).Encode(books)

}

func main() {
	r := mux.NewRouter()

	//mock data, todo implement ddb
	books = append(books, Book{ID: "1", Isbn:"448743", Title: "Book One", Author: &Author{Firstname: "jhon", Lastname: "Doe"}})
	books = append(books, Book{ID: "2", Isbn:"1234", Title: "Book Two", Author: &Author{Firstname: "bape", Lastname: "ppp"}})

	//route handlers
	r.HandleFunc("/api/books", getBooks). Methods("GET")
	r.HandleFunc("/api/books/{id}", getBooks). Methods("GET")
	r.HandleFunc("/api/books", getBooks). Methods("POST")
	r.HandleFunc("/api/books/{id}", getBooks). Methods("PUT")
	r.HandleFunc("/api/books/{id}", getBooks). Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", r))
}