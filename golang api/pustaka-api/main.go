package main

import (
	"log"
	"pustaka_api/book"
	"pustaka_api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "mysql:mysql@tcp(127.0.0.1:3306)/db_localhost?charset=utf8mb4&parseTime=True&loc=Local"
  	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db connection error")
	}

	db.AutoMigrate(&book.Book{})

	//REPOSITORY****
	// bookRepository := book.NewRepository(db)


	// books, err := bookRepository.FindAll()

	// for _, book := range books{
	// 	fmt.Println("Title:", book.Title)
	// }
	// book := book.Book{
	// 	Title: "peternakan",
	// 	Description: "good book",
	// 	Price: 10000,
	// 	Rating: 5,
	// 	Discount: 0,
	// }

	// bookRepository.Create(book)

	// //CRUD 
	// book.Repository := book.NewRepository(db)

	// CREATE
	// book := book.Book{}
	// book.Title = "ayam"
	// book.Price = 1000
	// book.Discount = 5
	// book.Rating = 5
	// book.Description = "buku bagus 2"

	// err = db.Create(&book).Error
	// if err != nil {
	// 		fmt.Println("================")
	// 		fmt.Println("error creat book")
	// 		fmt.Println("===============")
		
	// }

	// READ

	// var book book.Book
	// err = db.Debug().First(&book).Error
	// 	if err != nil {
	// 		fmt.Println("================")
	// 		fmt.Println("error finding book")
	// 		fmt.Println("===============")
	// 	}

	// 	fmt.Println("Title: ", book.Title)
	// 	fmt.Println("book object %v", book)


	// var books []book.Book

	// err = db.Debug().Where("title = ?", "man tiger").Find(&books).Error
	// 	if err != nil {
	// 		fmt.Println("================")
	// 		fmt.Println("error finding book")
	// 		fmt.Println("===============")
	// 	}

	// 	for _, b := range books {
	// 		fmt.Println("Title: ", b.Title)
	// 		fmt.Println("book object %v", b)
	// 	}


	// //UPDATE
	// var book book.Book

	// err = db.Debug().Where("id = ?",1).First(&book).Error
	// 	if err != nil {
	// 		fmt.Println("================")
	// 		fmt.Println("error finding book")
	// 		fmt.Println("===============")
	// 	}

	// 	book.Title = "Man Tiger"
	// 	err = db.Save(&book).Error
	// 	if err != nil {
	// 		fmt.Println("================")
	// 		fmt.Println("error updating book")
	// 		fmt.Println("===============")
	// 	}
		

	//DELETE
		// var book book.Book

		// err = db.Debug().Where("id = ?",1).First(&book).Error
		// if err != nil {
		// 	fmt.Println("================")
		// 	fmt.Println("error finding book")
		// 	fmt.Println("===============")
		// }


		// err = db.Delete(&book).Error
		// if err != nil {
		// 	fmt.Println("================")
		// 	fmt.Println("error Delete book")
		// 	fmt.Println("===============")
		// }

		bookRepository := book.NewRepository(db)
		bookService := book.NewService(bookRepository)
	// 	bookRequest := book.BookRequest{
	// 		Title: "perhutanan",
	// 		Price: "20000",
	// 	}

	// bookService.Create(bookRequest)
		bookHandler := handler.NewBookHandler(bookService)


	router := gin.Default()

	v1 := router.Group("/v1")

	// v1.GET("/", bookHandler.RootHandler)
	// v1.GET("/hello", bookHandler.HelloHandler)
	// v1.GET("/books/:id/:title", bookHandler.BooksHandler)
	// v1.GET("/query", bookHandler.QueryHandler)
	v1.POST("/books", bookHandler.CreateBook)
	v1.GET("books",bookHandler.GetBooks)
	v1.GET("/books/:id",bookHandler.GetBook)
	v1.PUT("/books/:id",bookHandler.UpdateBook)
	v1.DELETE("/books/:id",bookHandler.DeleteBook)

	router.Run(":8080")
}